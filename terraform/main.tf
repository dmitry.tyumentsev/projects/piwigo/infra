locals {
  instances = {
    # "web01"         = { cpu_cores = 2, memory_gb = 2 },
    "db01"          = { cpu_cores = 2, memory_gb = 2 },
    # "mon01"          = { cpu_cores = 2, memory_gb = 2 },
  }
}

module "yc-instance" {
  source                       = "git::https://gitlab.com/dmitry.tyumentsev/terraform-modules/yc-instance.git"
  for_each                     = local.instances
  instance_name                = each.key
  instance_hostname            = each.key
  instance_vpc_subnet_id       = yandex_vpc_subnet.vpc_subnet.id
  is_instance_preemptible      = true
  instance_cores               = each.value.cpu_cores
  instance_memory              = each.value.memory_gb
  instance_image_family        = "ubuntu-2004-lts"
  instance_labels              = { "ansible_group" : replace(each.key, "/\\d\\d/", "") }
  instance_ssh_public_key_path = var.ssh_public_key_path
}
