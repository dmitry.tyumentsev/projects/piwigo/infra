variable "resource_name_prefix" {
  description = "Prefix to define resource names"
  type        = string
  default     = "piwigo-"
}

variable "yc_iam_token" {
  description = "Yandex token `yc iam create-token`"
}

variable "yc_cloud_id" {
  description = "Yandex cloud id `yc config get cloud-id`"
}

variable "yc_folder_id" {
  description = "Yandex folder id `yc config get folder-id`"
}

variable "yc_region_zone" {
  description = "Yandex region `yc compute zone list`"
  type        = string
  default     = "ru-central1-b"
}

variable "vpc_subnet_v4_cidr_blocks" {
  description = "VPC subnet IPv4 CIDR blocks"
  type        = list(string)
  default     = ["10.129.0.0/24"]
}

variable "ssh_public_key_path" {
  type = string
  default = "id_ssh.pub"
}
